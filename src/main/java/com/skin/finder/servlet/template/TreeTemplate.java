/*
 * $RCSfile: TreeTemplate.java,v $
 * $Revision: 1.1 $
 *
 * JSP generated by JspCompiler-1.0.0
 */
package com.skin.finder.servlet.template;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * <p>Title: TreeTemplate</p>
 * <p>Description: </p>
 * @author JspKit
 * @version 1.0
 */
public class TreeTemplate extends com.skin.finder.web.servlet.JspServlet {
    private static final long serialVersionUID = 1L;
    private static final TreeTemplate instance = new TreeTemplate();


    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    public static void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        instance.service(request, response);
    }

    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=utf-8");
        OutputStream out = response.getOutputStream();

        out.write(_jsp_string_1, 0, _jsp_string_1.length);
        out.write(_jsp_string_2, 0, _jsp_string_2.length);
        out.write(_jsp_string_3, 0, _jsp_string_3.length);
        out.write(_jsp_string_4, 0, _jsp_string_4.length);
        out.write(_jsp_string_5, 0, _jsp_string_5.length);
        out.write(_jsp_string_6, 0, _jsp_string_6.length);
        out.write(_jsp_string_7, 0, _jsp_string_7.length);
        out.write(_jsp_string_8, 0, _jsp_string_8.length);
        out.write(_jsp_string_9, 0, _jsp_string_9.length);
        out.write(_jsp_string_10, 0, _jsp_string_10.length);
        out.write(_jsp_string_11, 0, _jsp_string_11.length);
        out.write(_jsp_string_12, 0, _jsp_string_12.length);
        out.write(_jsp_string_13, 0, _jsp_string_13.length);

        out.flush();
    }

    protected static final byte[] _jsp_string_1 = b("<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\r\n");
    protected static final byte[] _jsp_string_2 = b("<meta http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n<meta http-equiv=\"Cache-Control\" content=\"no-cache\"/>\r\n");
    protected static final byte[] _jsp_string_3 = b("<meta http-equiv=\"Expires\" content=\"0\"/>\r\n<title>Finder - Powered by Finder</title>\r\n");
    protected static final byte[] _jsp_string_4 = b("<link rel=\"shortcut icon\" href=\"?action=res&path=/finder/images/favicon.png\"/>\r\n");
    protected static final byte[] _jsp_string_5 = b("<link rel=\"stylesheet\" type=\"text/css\" href=\"?action=res&path=/finder/css/finder.css\"/>\r\n");
    protected static final byte[] _jsp_string_6 = b("<link rel=\"stylesheet\" type=\"text/css\" href=\"?action=res&path=/htree/css/htree.css\"/>\r\n");
    protected static final byte[] _jsp_string_7 = b("<script type=\"text/javascript\" src=\"?action=res&path=/finder/jquery-1.7.2.min.js\"></script>\r\n");
    protected static final byte[] _jsp_string_8 = b("<script type=\"text/javascript\" src=\"?action=res&path=/htree/htree.js\"></script>\r\n");
    protected static final byte[] _jsp_string_9 = b("<script type=\"text/javascript\" src=\"?action=res&path=/htree/htree.util.js\"></script>\r\n");
    protected static final byte[] _jsp_string_10 = b("<script type=\"text/javascript\" src=\"?action=res&path=/finder/tree.js\"></script>\r\n");
    protected static final byte[] _jsp_string_11 = b("</head>\r\n<body>\r\n<div class=\"left-nav\">\r\n    <div class=\"menu-body\" style=\"padding-left: 8px; overflow-x: auto; overflow-y: scroll;\">\r\n");
    protected static final byte[] _jsp_string_12 = b("        <div id=\"htree\" class=\"htree\" style=\"margin-top: 10px; white-space: nowrap;\"></div>\r\n");
    protected static final byte[] _jsp_string_13 = b("    </div>\r\n</div>\r\n</body>\r\n</html>\r\n");

}
