<%@ page contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>Finder - Powered by Finder</title>
<link rel="shortcut icon" href="?action=res&path=/finder/images/favicon.png"/>
<link rel="stylesheet" type="text/css" href="?action=res&path=/admin/css/form.css"/>
<script type="text/javascript" src="?action=res&path=/finder/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="?action=res&path=/admin/base.js"></script>
<script type="text/javascript">
<!--
jQuery(function() {
    jQuery("button[name=submit]").click(function() {
        var userName = jQuery.trim(jQuery("input[name=userName]").val());
        var hostName = jQuery.trim(jQuery("select[name=hostName]").val());

        if(userName.length < 1) {
            alert("请输入用户名！");
            return;
        }

        if(hostName.length < 1) {
            alert("请选择主机！");
            return;
        }

        var params = [];
        params[params.length] = "userName=" + encodeURIComponent(userName);
        params[params.length] = "hostName=" + encodeURIComponent(hostName);

        jQuery.ajax({
            "type": "get",
            "url": "?action=admin.policy.host.add&host=" + encodeURIComponent(hostName),
            "dataType": "json",
            "data": params.join("&"),
            "error": function() {
                alert("系统错误，请稍后再试！");
            },
            "success": function(response) {
                if(response.status != 200) {
                    alert(response.message);
                    return;
                }
                alert("添加成功，您可以继续添加其他主机或者返回到上一个页面！");
            }
        });
    });
});
//-->
</script>
</head>
<body>
<div class="menu-bar">
    <a class="button" href="javascript:void(0)" onclick="window.history.back();"><span class="back"></span>返回</a>
    <a class="button" href="javascript:void(0)" onclick="window.location.reload();"><span class="refresh"></span>刷新</a>
    <span class="seperator"></span>
</div>
<%
    String[] hosts = (String[])(request.getAttribute("hosts"));

    if(hosts == null) {
        hosts = new String[0];
    }
%>
<div class="form">
    <div class="form-row">
        <div class="form-label">User Name：</div>
        <div class="form-c300">
            <div class="form-field">
                <input name="userName" type="text" class="text w200" placeholder="User Name" value=""/>
            </div>
        </div>
        <div class="form-m300">
            <div class="form-comment">User Name .</div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-label">Host Name</div>
        <div class="form-c300">
            <div class="form-field">
                <select name="hostName" type="text" class="select w200" title="主机">
                    <option value="">请选择主机</option>
                <%
                    for(String hostName : hosts) {
                %>
                    <option value="<%=hostName%>"><%=hostName%></option>
                <%
                    }
                %>
                </select>
            </div>
        </div>
        <div class="form-m300">
            <div class="form-comment">Host Name .</div>
        </div>
    </div>

    <div class="button">
        <button name="submit" class="button ensure">保 存</button>
    </div>
</div>
<div id="pageContext" style="display: none;" contextPath="${contextPath}"></div>
</body>
</html>
