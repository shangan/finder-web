<%@ page contentType="text/html; charset=utf-8"%>
<%@ page import="com.skin.finder.config.ConfigFactory"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>Login - Powered by Finder</title>
<link rel="shortcut icon" href="?action=res&path=/finder/images/favicon.png"/>
<link rel="stylesheet" type="text/css" href="?action=res&path=/finder/css/user.css"/>
<script type="text/javascript" src="?action=res&path=/finder/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="?action=res&path=/finder/jsencrypt.min.js"></script>
<script type="text/javascript" src="?action=res&path=/finder/signin.js"></script>
</head>
<body style="background: url(?action=res&path=/finder/images/7a4f.jpg) center center / cover no-repeat fixed;">
<div class="apptop">
    <a href="http://www.finderweb.net" target="_blank"><div class="logo" title="http://www.finderweb.net"></div></a>
    <div style="margin-left: 300px; height: 60px;">
        <div style="height: 28px;"></div>
        <div class="menu-bar"></div>
    </div>
</div>
<div class="wrap">
    <div class="login-container">
        <div class="login-panel">
            <h2>用户登录</h2>
            <div class="row"><input id="s1" type="text" class="text" spellcheck="false" placeholder="UserName" value="<%=ConfigFactory.getDemoUserName()%>"/></div>
            <div class="row"><input id="s2" type="password" class="text" placeholder="Password" value="<%=ConfigFactory.getDemoPassword()%>"/></div>
            <div class="row"><input id="submit" type="button" class="button" value="登录"/></div>
        </div>
    </div>
</div>
<div style="display: none;">
    <textarea id="publicKey"><%=ConfigFactory.getPublicKey()%></textarea>
</div>
<div style="display: none;"><%this.write(out, ConfigFactory.getAccessCode(), false);%></div>
<!-- http://www.finderweb.net -->
</body>
</html>
